import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://dog.ceo/api/',
});

export const dogsByBreed = async (
  breed?: string
): Promise<{
  status: 'success' | 'failure';
  message: Array<string>;
}> => {
  const { data } = await instance.get(`/breed/${breed}/images`);
  return data;
};
