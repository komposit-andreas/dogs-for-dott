import React, { useState } from 'react';
import { DogUploader } from '../../components/DogUploader/DogUploader';
import { ImageGrid } from '../../components/ImageGrid/ImageGrid';
import { Page } from '../../components/Page/Page';
import { StatusBar } from '../../components/StatusBar/StatusBar';
import { AppStatus, DogImage } from '../../types';

export const Home = (): JSX.Element => {
  const homeText = 'Home';
  const [dogImages, setDogImages] = useState<DogImage[]>([]);
  const [breed, setBreed] = useState<string | null>(null);
  const [appStatus, setAppStatus] = useState<AppStatus>({
    activeStatus: 'pending',
    message: "Click to upload a picture of the dog you'd like to identify",
  });

  return (
    <Page description={homeText} keywords={homeText} title={homeText}>
      <DogUploader setBreed={setBreed} setAppStatus={setAppStatus} />
      <StatusBar appStatus={appStatus} />
      <ImageGrid
        dogImages={dogImages}
        setDogImages={setDogImages}
        breed={breed}
        setAppStatus={setAppStatus}
      />
    </Page>
  );
};
