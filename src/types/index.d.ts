export interface AppStatus {
  activeStatus: 'error' | 'success' | 'pending' | 'loading';
  message: string;
}

export interface DogImage {
  url: string;
  loaded: boolean;
}
