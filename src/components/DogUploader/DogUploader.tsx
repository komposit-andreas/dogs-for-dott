import * as mobilenet from '@tensorflow-models/mobilenet';
import React, { Dispatch, SetStateAction, useState, useEffect, ChangeEvent, useRef } from 'react';
import dog from '../../assets/images/dog-outline.png';
import { AppStatus } from '../../types';

interface Props {
  setBreed: Dispatch<SetStateAction<string | null>>;
  setAppStatus: Dispatch<SetStateAction<AppStatus>>;
}

export const DogUploader: React.FC<Props> = ({ setBreed, setAppStatus }) => {
  const [image, setImage] = useState<string | null>(null);
  const imageNode = useRef<HTMLImageElement>(null);

  useEffect(() => {
    const classify = async () => {
      if (!image || !imageNode.current) return;
      setAppStatus({
        activeStatus: 'loading',
        message: 'Classifying image...',
      });
      const model = await mobilenet.load({
        version: 2,
        alpha: 1.0,
      });
      const classificationResult = await model.classify(imageNode.current);
      const detectedBreed = classificationResult?.[0]?.className?.toLowerCase()?.split(' ')?.pop();
      if (!detectedBreed) {
        throw new Error();
      }
      setAppStatus({
        activeStatus: 'loading',
        message: `Image classification complete: ${detectedBreed} detected. Loading pictures`,
      });
      setBreed(detectedBreed);
    };
    try {
      classify();
    } catch (error) {
      setAppStatus({
        activeStatus: 'error',
        message: 'Dog classification failed',
      });
    }
  }, [image, setBreed, setAppStatus]);

  const onFileChange = async (e: ChangeEvent<HTMLInputElement>) => {
    const file = e?.target?.files?.[0];
    if (!file) return;
    const reader = new FileReader();
    reader.onloadend = () => {
      setImage(reader.result as string);
    };
    reader.readAsDataURL(file);
  };

  return (
    <div className="rounded-lg overflow-hidden my-5">
      <label htmlFor="image-upload">
        <img
          ref={imageNode}
          src={image || dog}
          alt=""
          className="mx-auto rounded-lg h-60 cursor-pointer"
          title="click to upload"
        />
        <input
          id="image-upload"
          className="hidden"
          type="file"
          onChange={onFileChange}
          accept="image/png, image/gif, image/jpeg"
        />
      </label>
    </div>
  );
};
