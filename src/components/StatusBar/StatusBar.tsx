import React from 'react';
import { AppStatus } from '../../types';
import { Spinner } from '../Spinner/Spinner';

interface Props {
  appStatus: AppStatus;
}

export const StatusBar: React.FC<Props> = ({ appStatus }) => {
  const { activeStatus, message } = appStatus;
  const { bg, text, border } = {
    error: {
      bg: 'bg-red-100',
      text: 'text-red-900',
      border: 'border-red-900',
    },
    pending: {
      bg: 'bg-blue-100',
      text: 'text-blue-900',
      border: 'border-blue-900',
    },
    loading: {
      bg: 'bg-blue-100',
      text: 'text-blue-900',
      border: 'border-blue-900',
    },
    success: {
      bg: 'bg-green-100',
      text: 'text-green-900',
      border: 'border-green-900',
    },
  }[activeStatus];

  return (
    <div className={`mx-auto border rounded-lg text-center ${bg} ${border}`}>
      <span className="my-3 mx-auto inline-block">
        {activeStatus === 'loading' && <Spinner className={`${text}-900 inline`} />}
        {message}
      </span>
    </div>
  );
};
