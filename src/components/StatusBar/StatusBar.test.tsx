import { render, screen, waitFor, act, fireEvent } from '@testing-library/react';
import React from 'react';
import { StatusBar } from './StatusBar';

test('renders the message', async () => {
  const container = render(
    <StatusBar appStatus={{ activeStatus: 'pending', message: 'this works' }} />
  );
  const msg = await waitFor(() => screen.getByText(/this works/));
  expect(msg).toBeInTheDocument();
});

test('is red when status error', async () => {
  const { container } = render(
    <StatusBar appStatus={{ activeStatus: 'error', message: 'this works' }} />
  );
  expect(container.firstChild).toHaveClass('bg-red-100');
});

test('is blue when status pending input', async () => {
  const { container } = render(
    <StatusBar appStatus={{ activeStatus: 'pending', message: 'this works' }} />
  );
  expect(container.firstChild).toHaveClass('bg-blue-100');
});

test('is green when status success input', async () => {
  const { container } = render(
    <StatusBar appStatus={{ activeStatus: 'success', message: 'this works' }} />
  );
  expect(container.firstChild).toHaveClass('bg-green-100');
});

test('shows a spinner when status loading', async () => {
  const { container } = render(
    <StatusBar appStatus={{ activeStatus: 'loading', message: 'this works' }} />
  );
  const spinner = screen.getByTitle(/loading/);
  expect(spinner).toBeInTheDocument();
});
