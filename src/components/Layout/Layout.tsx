import React, { PropsWithChildren, Suspense } from 'react';
import { ErrorBoundary } from '../ErrorBoundary/ErrorBoundary';
import { Loading } from '../Loading/Loading';

const Container = ({ children }: PropsWithChildren<Record<string, unknown>>): JSX.Element => (
  <ErrorBoundary>
    <Suspense fallback={<Loading />}>{children}</Suspense>
  </ErrorBoundary>
);

export const Layout = ({ children }: PropsWithChildren<Record<string, unknown>>): JSX.Element => (
  <>
    <Container>{children}</Container>
  </>
);
