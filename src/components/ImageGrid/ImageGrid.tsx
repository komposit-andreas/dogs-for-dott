import React, { useEffect, useRef, Dispatch, SetStateAction, useState } from 'react';
import { dogsByBreed } from '../../common/apis/dog';
import { DogImage, AppStatus } from '../../types';
import { Spinner } from '../Spinner/Spinner';

interface Props {
  dogImages: DogImage[];
  setDogImages: Dispatch<SetStateAction<DogImage[]>>;
  breed: string | null;
  setAppStatus: Dispatch<SetStateAction<AppStatus>>;
}

export const ImageGrid: React.FC<Props> = ({ dogImages, setDogImages, breed, setAppStatus }) => {
  const [limit, setLimit] = useState(15);
  const list = useRef<HTMLUListElement>(null);

  useEffect(() => {
    const getData = async () => {
      try {
        if (!breed) return;
        setAppStatus({
          activeStatus: 'loading',
          message: `Loading pictures of dog type ${breed}`,
        });
        setLimit(15);
        const { status, message } = await dogsByBreed(breed);

        if (status !== 'success') throw new Error();
        setDogImages(message.map((url) => ({ url, loaded: false })));
        setAppStatus({
          activeStatus: 'success',
          message: `${breed}`,
        });
      } catch (e) {
        setDogImages([]);
        setAppStatus({
          activeStatus: 'error',
          message: `Dog api failed to return images of ${breed}`,
        });
      }
    };
    getData();
  }, [breed, setDogImages, setAppStatus]);

  useEffect(() => {
    const loadMore = () => {
      if (!list.current) return;
      // Bottom reached?
      const scrollHeight = window.innerHeight + window.scrollY;
      const gridOffsetTop = list.current.clientHeight + list.current.offsetTop;
      if (gridOffsetTop - scrollHeight > 0) return;
      // load 6 more
      setLimit(limit + 6);
    };
    window.addEventListener('scroll', loadMore);
    return () => window.removeEventListener('scroll', loadMore);
  }, [setLimit, limit]);
  const toggleLoaded = (idx: number) => () =>
    setDogImages(dogImages.map((img, i) => (i === idx ? { url: img.url, loaded: true } : img)));

  return (
    <ul
      className="my-5 grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8"
      id="img-list"
      ref={list}
    >
      {dogImages.slice(0, limit).map((img, idx) => (
        <li key={img.url} className="relative">
          <div className="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
            <img
              onLoad={toggleLoaded(idx)}
              src={img.url}
              alt=""
              className={`object-cover pointer-events-none group-hover:opacity-75 ${
                img.loaded || 'hidden'
              }`}
            />
            <div className={`bg-red-900 w-full h-full ${img.loaded ? 'hidden' : ''}`}>
              <Spinner className="text-white" />
            </div>
          </div>
        </li>
      ))}
    </ul>
  );
};
