import { render, screen, waitFor, act, fireEvent } from '@testing-library/react';
import React from 'react';
import dogApiResponse from '../../test/mockdata/dogApiResponse.json';
import { ImageGrid } from './ImageGrid';

const dogImages = dogApiResponse.message.map((url) => ({
  url,
  loaded: false,
}));

test('renders empty list when no images present', async () => {
  render(<ImageGrid dogImages={[]} />);
  const list = screen.getByRole('list');
  const items = screen.queryAllByRole('listitem'); // returns empty array when none present
  expect(list).toBeInTheDocument();
  expect(items.length).toBe(0);
});

test('renders 15 image elements initially', async () => {
  act(() => {
    render(<ImageGrid dogImages={dogImages} />);
  });
  const list = screen.getByRole('list');
  const items = screen.queryAllByRole('listitem');
  expect(list).toBeInTheDocument();
  expect(items.length).toBe(15);
});

// test('loads 6 more items whens scrolling down', async () => {
//   act(() => {
//     render(<ImageGrid dogImages={dogImages} />);
//     // @TODO Fix mocking scroll down
//     fireEvent.scroll(window, { target: { scrollY: 1500 } });
//   });
//   const items2 = screen.queryAllByRole('listitem');
//   expect(items2.length).toBe(21);
// });
