# Dogs for Dott

## Introduction

Small SPA which lets users upload images. If a dog breed can be
identified from the image a list of dogs of that breed is then fetched
and shown.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Installation

`git clone git@bitbucket.org:komposit-andreas/dogs-for-dott.git`
`cd dogs-for-dott`
`npm install`
`npm run start`

## Demo

A working Demo of the application can be found at https://dogs-for-dott.vercel.app/

## Design

From the perspective of the UI this application has three distinct
responsibilities.

1.  Provide a way for the user to upload an image in order to determine
    the dog breed.
2.  Given a dog breed, to provide a gallery of images of dogs of this
    breed.
3.  Providing the user with instructions & feedback on the status of the
    app.

These responsibilities are handled each by their own component
(`DogUploader`, `ImageGrid`, and `StatusBar` respectively). Local
state is isolated within these components whereas shared state is
initialized in the overlying `Home.tsx` component.

## Known issues / Would improve given more time

- Move config to environment (urls)
- Fix styling of image in loading state in grid.
- Implement missing tests
