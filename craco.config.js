// craco.config.js
/* eslint-disable @typescript-eslint/no-var-requires */
const autoprefixer = require('autoprefixer');
const tailwind = require('tailwindcss');
/* eslint-enable @typescript-eslint/no-var-requires */

module.exports = {
  style: {
    postcss: {
      plugins: [tailwind, autoprefixer],
    },
  },
};
